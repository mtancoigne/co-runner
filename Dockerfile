FROM php:5.6

# Node repo
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -

# System packages
RUN apt-get update
RUN apt-get install git unzip libmcrypt-dev libgd-dev libxml2-dev libzip-dev libcurl4-openssl-dev nodejs mongodb --assume-yes

# Common PHP ext
RUN docker-php-ext-install mcrypt gd mbstring xml curl zip
RUN pecl config-set php_ini "${PHP_INI_DIR}/php.ini"

# Mongodb ext
RUN pecl install mongodb
RUN docker-php-ext-enable mongodb

# Start services
RUN service mongodb start

# Install Yarn
RUN npm install -g yarn